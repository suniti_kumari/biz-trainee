package Test;

import org.apache.log4j.BasicConfigurator;


public class Demo {
static final Logger logger=Logger.getLogger(Demo.class);
	public static void main(String[] args) {
		Demo d=new Demo();
		d.log();
	}
	public void log(){
		BasicConfigurator.configure();
		logger.info("this is logger information");
		logger.debug("this is logger debug");
		logger.error("this is logger error information");

	}

}