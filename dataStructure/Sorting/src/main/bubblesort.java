package Bubble.Sorting;
import org.apache.log4j.BasicConfigurator;
import java.util.Scanner;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
class bubblesort {
	static final Logger logger=Logger.getLogger(bubblesort.class);
  public static void main(String []args) {
	  bubblesort b=new bubblesort();
	  b.sort();
  }
  public void sort(){
    int n, c, d, swap;
    Scanner in = new Scanner(System.in);
 
    logger.info("Input number of integers to sort");
    n = in.nextInt();
 
    int array[] = new int[n];
 
    logger.info("Enter " + n + " integers");
 
    for (c = 0; c < n; c++) 
      array[c] = in.nextInt();
 
    for (c = 0; c < ( n - 1 ); c++) {
      for (d = 0; d < n - c - 1; d++) {
        if (array[d] > array[d+1]) 
        {
          swap       = array[d];
          array[d]   = array[d+1];
          array[d+1] = swap;
        }
      }
    }
 
    logger.info("Sorted list of numbers");
 
    for (c = 0; c < n; c++) 
      logger.info(array[c]);
  }
}

