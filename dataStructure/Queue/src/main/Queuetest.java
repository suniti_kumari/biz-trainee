package Queue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
public class Queuetest{  
	static final Logger logger=Logger.getLogger(Queuetest.class);
	 private static final int capacity = 3;  
	 int arr[] = new int[capacity];  
	 int size = 0;  
	 int top = -1;  
	 int rear = 0;  
	  
	 public void push(int pushedElement) {  
	  if (top < capacity - 1) {  
	   top++;  
	   arr[top] = pushedElement;  
	  logger.info("Element " + pushedElement   + " is pushed to Queue !");  
	   display();  
	  } else {  
	  logger.info("Overflow !");  
	  }  
	  
	 }  
	  
	 public void pop() {  
	  if (top >= rear) {  
	   rear++;  
	   logger.info("Pop operation done !");  
	   display();  
	  } else {  
	   logger.info("Underflow !");  
	  }  
	 }  
	  
	 public void display() {  
	  if (top >= rear) {  
	   System.out.println("Elements in Queue : ");  
	   for (int i = rear; i <= top; i++) {  
	    logger.info(arr[i]);  
	   }  
	  }
	  public static void main(String[] args) {  
		  Queuetest q = new Queuetest();  
		  q.pop();  
		  q.push(23);  
		  q.push(2);  
		  q.push(73);  
		  q.push(21);  
		  q.pop();  
		  q.pop();  
		  q.pop();  
		  q.pop(); 
		 
		 }  
		  
		}  
	 }  
	  
