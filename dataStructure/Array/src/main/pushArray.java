
public class pushArray {
	    protected int head[];
	    protected int pointer;

	    public pushArray(int Capacity){
	        head = new int[10];
	        pointer = -1;
	    }
	    
		public boolean isEmpty(){
	        return pointer == -1;
	    }
	    public void push(int i){
	        if(pointer+1 < head.length)
	            head[++pointer] = i;
	    }
	    public int pop(){
	        if(isEmpty())
	            return 0;
	        return head[pointer--];
	    }
	
}
