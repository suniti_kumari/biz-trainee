import java.io.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
class pushArrayTest{
	static final Logger logger=Logger.getLogger(pushArrayTest.class);
    public static void main(String[] args){
    	pushArrayTest s=new pushArrayTest();
    	s.Demo();
    }
    public void Demo(){
    	BasicConfigurator.configure();
        pushArray s = new pushArray(10);
        int i,j;
        logger.info("starting...");
        for(i=0;i<10;i++){
            j = (int)(Math.random() * 100);
            s.push(j);
            logger.info("push: " + j);
        }
        while(!s.isEmpty()){
           logger.info("pop: " + s.pop());
        }
        logger.info("Done ;-)");
    }
}
