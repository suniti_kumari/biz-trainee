package Stack;
import java.io.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

class Stackmain{
	static final Logger logger=Logger.getLogger(Stackmain.class);
    public static void main(String[] args){
    	Stackmain s=new Stackmain();
    	s.Demo();
    }
    	public void Demo(){
    	BasicConfigurator.configure();
        PushStack s = new PushStack(10);
        int i,j;
        logger.info("starting...");
        for(i=0;i<10;i++){
            j = (int)(Math.random() * 100);
            s.push(j);
            logger.info("push: " + j);
        }
        while(!s.isEmpty()){
           logger.info("pop: " + s.pop());
        }
        logger.info("Done");
    }
}